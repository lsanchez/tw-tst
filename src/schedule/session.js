const sessionRemain = require('./session-remaining')

function defineTalksbySession(session_length, talks) {

    let remaining = session_length
    let talks_session = []
    let talks_over = []

    talks.forEach(function(talk) {

        const session_remain = sessionRemain(remaining, [talk])

        if (session_remain >= 0) {
            talks_session.push(talk)
            remaining = session_remain
        } else {
            talks_over.push(talk)
        }

    })

    return [talks_session, talks_over]

}

module.exports = defineTalksbySession