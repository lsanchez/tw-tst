function formatTalk(raw) {
    const name_clean = raw.replace('\r\n', '')
    const splited = name_clean.split(' ')
    const lenght = formatLenght(splited)
    
    return [name_clean, lenght];
}

function formatLenght(splited) {
    let lenght = splited.pop().replace('\n', '')

    if (lenght === 'lightning') {
        lenght = 5
    } else {
        lenght = parseInt(lenght.slice(0, -3), 10)
    }
    
    return lenght
}

module.exports = {
    format: formatTalk, 
    lenght: formatLenght
}