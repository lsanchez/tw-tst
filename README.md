# Awesome Conference Track Management

Welcome to your Awesome Conference Track Managemente

## Config

The track's configuration of your awesome conference is at `/config/tracks.json`. You can put how much tracks and sessions you need!

## Prerequisites

- Node
- Yarn to tests

## To run

`yarn run` ou `node index.js`

## To install

`yarn install`

## To test

`yarn test`

## To exit

`ctrl + c`

## Explicação

O algoritmo começa validando a entrada do usuário. Se não tiver um valor em XXmin ou lightning ele retorna um erro explicando a formatação correta das talks.

Depois ele pega a talk formatada e adiciona em uma lista de talks. Chamei essa lista de `db` pra ficar simples caso precise encaixar um banco de dados.

Essa lista é interada e talk por talk é adicionada em uma session se tiver espaço. O algoritmo não procura um melhor lugar, apenas deixa diretamente na primeira posição que encontra.

Caso não caiba na sessão, o algoritmo continua interando as talks nas próximas sessões e tracks.

Caso não caiba na conferência, o algoritmo retorna as talks que não couberam.

A formatação da confererência é à partir do arquivo `config/tracks.json`. Usei esse arquivo de configuração para deixar mais possibilidades em aberto. Assim, ela pode ter N tracks e N sessões. Deixei um espaço "hard coded" para o "final_event", pois como temos horários específicos para término de cada sessão não tem porque atrelá-lo a um horário especifico da grade, mas caso necessário também pode ser calculado.

As sessões de cada track tem horários de início e duração, ambas em minutos. Isso servirá para o algoritmo saber o espaço de cada sessão e os horários que deve printar na tela.

## Melhorias

- Começaria refatorando o arquivo `src/schedule/track.js` pois tem 3 forEachs ali. Daria pra separar em funções menores
- Usei arrays para a formatação das talks pois é mais simples a ordenação caso precise em um futuro, mas poderia ser trocado pra objetos para melhorar a legibilidade e entendimento do código
- Adicionaria testes unitários para as entradas do usuário via linha de comando, hoje quem cumpre esse papel é o `cli.test.js`
- Adicionaria logs / debug