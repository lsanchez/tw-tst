const grid = require('./grid')

describe('return grid formatted', () => {

    it('should return a grid', () => {

        const starts_at = 0
        
        const talks = [
            ['Writing Fast Tests Against Enterprise Rails 60min', 60],
            ['Overdoing it in Python 45min', 45]
        ]

        const result = [
            '12:00AM Writing Fast Tests Against Enterprise Rails 60min',
            '01:00AM Overdoing it in Python 45min'
        ]

        expect(grid(starts_at, talks)).toEqual(result)
    })

    it('should return a grid (2)', () => {

        const starts_at = 0
        
        const talks = [
            ['Writing Fast Tests Against Enterprise Rails 60min', 60],
            ['Overdoing it in Python 45min', 45],
            ['Lua for the Masses 30min', 30]
        ]

        const result = [
            '12:00AM Writing Fast Tests Against Enterprise Rails 60min',
            '01:00AM Overdoing it in Python 45min',
            '01:45AM Lua for the Masses 30min'
        ]

        expect(grid(starts_at, talks)).toEqual(result)
    })

})