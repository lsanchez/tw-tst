const valid_talk = require('../validate/talk')
const format_talk = require('../format/talk')
const db = require('../db/db')
const formatSchedule = require('../schedule/track')

function welcomeMessage() {
    console.log('--------------------------------------------------')
    console.log('- WELCOME TO AWESOME CONFERENCE TRACK MANAGEMENT -')
    console.log('--------------------------------------------------')
    console.log('Please add a talk to schedule')
    return
}

function runSchedule(talk, config, database) {
    
    const validate_talk = valid_talk.format(talk)

    if (validate_talk) {
        const formatted_talk = format_talk.format(talk)
        const talks = db.update(database, formatted_talk)
        const conference = formatSchedule(config, talks)
        printConference(conference)
        return conference
    } else {
        return false
    }
}

function printConference(conference) {
    console.log('---------------------------------------------')
    console.log('---- SCHEDULE OF YOUR AWESOME CONFERENCE ----')
    console.log('---------------------------------------------')
    conference.forEach(item => {
        console.log(item)
    })
    return
}


module.exports = {
    welcomeMessage,
    runSchedule
}