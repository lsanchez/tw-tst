function formatDate(minutes) {

    let min = serialize(minutes % 60)
    let hrs = Math.floor(minutes / 60)
    let period = definePeriod(hrs)

    hrs = convertTo12HoursClock(hrs)

    return `${hrs}:${min}${period}`
}

function serialize(value) {
    if (value < 10) {
        value = '0' + value
    }
    return String(value)
}

function definePeriod(hrs) {
    return (hrs <= 11 || hrs >= 23) ? "AM" : "PM"
}

function convertTo12HoursClock(hrs) {
    if (hrs > 12) {
        hrs = hrs - 12
    }
    if (hrs === 0) {
        hrs = '12'
    }
    return serialize(hrs)
}

module.exports = {
    formatDate,
    serialize,
    convertTo12HoursClock,
    definePeriod
}