const db = require('./db')

describe('update talks', () => {

    it('should update a array', () => {
        const old = [['Writing Fast Tests Against Enterprise Rails', 60]]
        const raw = ['Rails for Python Developers', 5]
        const formatted = [
            ['Writing Fast Tests Against Enterprise Rails', 60],
            ['Rails for Python Developers', 5]
        ]

        expect(db.update(old, raw)).toEqual(formatted)
    })

    it('should update a array (2)', () => {
        const old = [['Writing Fast Tests Against Enterprise Rails', 60]]
        const raw = ['Overdoing it in Python', 45]
        const formatted = [
            ['Writing Fast Tests Against Enterprise Rails', 60],
            ['Overdoing it in Python', 45]
        ]

        expect(db.update(old, raw)).toEqual(formatted)
    })

    it('should update a array (3)', () => {
        const old = [
            ['Writing Fast Tests Against Enterprise Rails', 60],
            ['Overdoing it in Python', 45]
        ]
        const raw = ['Overdoing it in Python PT 2', 45]
        const formatted = [
            ['Writing Fast Tests Against Enterprise Rails', 60],
            ['Overdoing it in Python', 45],
            ['Overdoing it in Python PT 2', 45]
        ]

        expect(db.update(old, raw)).toEqual(formatted)
    })

})