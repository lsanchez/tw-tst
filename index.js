const cli = require('./src/cli/cli')
const config = require('./config/tracks')
const database = []

process.stdin.setEncoding('utf8')

process.stdin.on('readable', () => {

  const talk = process.stdin.read()

  if(talk && talk != '\n') {
    cli.runSchedule(talk, config, database)
  }
  
  return
  
});


cli.welcomeMessage()