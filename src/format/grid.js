const format = require('../format/date')

function grid(session_starts_at, talks) {

    let grid = []
    let current_minutes = session_starts_at

    talks.forEach(talk => {

        const name = talk[0]
        const lenght = talk[1]
        const date = format.formatDate(current_minutes)
        const print = `${date} ${name}`
        
        current_minutes = current_minutes + lenght
        grid.push(print)

    });

    return grid
}

module.exports = grid