const format = require('./date')

describe('return date formatted', () => {

    it('should return a date formatted 12:45AM', () => {
        const minutes = 45
        expect(format.formatDate(minutes)).toEqual('12:45AM')
    })

    it('should return a date formatted 01:15AM', () => {
        const minutes = 75
        expect(format.formatDate(minutes)).toEqual('01:15AM')
    })

    it('should return a date formatted 01:15AM', () => {
        const minutes = 600
        expect(format.formatDate(minutes)).toEqual('10:00AM')
    })

    it('should return a date formatted 12:20PM', () => {
        const minutes = 740
        expect(format.formatDate(minutes)).toEqual('12:20PM')
    })

    it('should return a date formatted 01:15AM', () => {
        const minutes = 780
        expect(format.formatDate(minutes)).toEqual('01:00PM')
    })

})


describe('return min or hours formatted with 0 if needed', () => {

    it('should return 05', () => {
        const value = 5
        expect(format.serialize(value)).toEqual('05')
    })

    it('should return 11', () => {
        const value = 11
        expect(format.serialize(value)).toEqual('11')
    })

});


describe('return AM or PM', () => {

    it('should return AM', () => {
        const hrs = 5
        expect(format.definePeriod(hrs)).toEqual('AM')
    })

    it('should return PM', () => {
        const hrs = 13
        expect(format.definePeriod(hrs)).toEqual('PM')
    })

});
