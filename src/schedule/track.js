const defineTalksSession = require('../schedule/session')
const formatGrid = require('../format/grid')

function formatGridByTrack(config_tracks, talks) {

    let conference_schedule = []
    
    config_tracks.forEach(track => {

        conference_schedule.push(track.name)

        track.sessions.forEach(function(session) {

            const returned_talks = defineTalksSession(session.lenght, talks)
            const this_session_talks = returned_talks[0]
            const next_session_talks = returned_talks[1]

            const session_grid = formatGrid(session.starts_at, this_session_talks)

            session_grid.forEach(function(talk) {
                conference_schedule.push(talk.replace('\n', ''))
            })
           
            conference_schedule.push(session.final_event)
            talks = next_session_talks

        })

    })

    if (talks[0] != undefined) {
        conference_schedule.push('TALKS WITHOUT SPACE IN YOUR CONFERENCE:')
        talks.forEach(function(talk) {
            conference_schedule.push(talk[0])
        })
    }

    return conference_schedule
}

module.exports = formatGridByTrack