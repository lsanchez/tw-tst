const sessionRemain = require('./session-remaining')

describe('schedule talks', () => {

    it('should return the remaining time of session 85min', () => {

        const session_length = 180

        const talks = [
            ['Writing Fast Tests Against Enterprise Rails', 60],
            ['Rails for Python Developers', 5],
            ['Lua for the Masses', 30]
        ]

        expect(sessionRemain(session_length, talks)).toEqual(85)

    })

    it('should return the remaining time of session 40min', () => {

        const session_length = 180

        const talks = [
            ['Writing Fast Tests Against Enterprise Rails', 60],
            ['Rails for Python Developers', 5],
            ['Lua for the Masses', 30],
            ['Ruby Errors from Mismatched Gem Versions', 45]
        ]

        expect(sessionRemain(session_length, talks)).toEqual(40)

    })
 
})