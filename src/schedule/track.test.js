const formatGridByTrack = require('./track')

describe('return date formatted', () => {

    it('should return a event grade without talks', () => {
        
        const tracks = [
            { 
                name: 'Track 1',
                sessions: [{
                    starts_at: 0,
                    lenght: 5,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 5,
                    final_event: '05:00PM Networking Event'
                }]
            },
            { 
                name: 'Track 2',
                sessions: [{
                    starts_at: 0,
                    lenght: 5,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 5,
                    final_event: '05:00PM Networking Event'
                }]
            }
        ]

        const talks = []

        const result = [
            'Track 1',
            '12:00PM Lunch',
            '05:00PM Networking Event',
            'Track 2',
            '12:00PM Lunch',
            '05:00PM Networking Event',
        ]

        expect(formatGridByTrack(tracks, talks)).toEqual(result)

    })

    it('should return a grade by track', () => {
        
        const tracks = [
            { 
                name: 'Track 1',
                sessions: [{
                    starts_at: 0,
                    lenght: 5,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 5,
                    final_event: '05:00PM Networking Event'
                }]
            },
            { 
                name: 'Track 2',
                sessions: [{
                    starts_at: 0,
                    lenght: 5,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 5,
                    final_event: '05:00PM Networking Event'
                }]
            }
        ]

        const talks = [
            ['Writing Fast Tests Against Enterprise Rails', 5],
            ['Rails for Python Developers', 5],
            ['Lua for the Masses', 5],
            ['Ruby Errors from Mismatched Gem Versions', 5]
        ]

        const result = [
            'Track 1',
            '12:00AM Writing Fast Tests Against Enterprise Rails',
            '12:00PM Lunch',
            '01:00PM Rails for Python Developers',
            '05:00PM Networking Event',
            'Track 2',
            '12:00AM Lua for the Masses',
            '12:00PM Lunch',
            '01:00PM Ruby Errors from Mismatched Gem Versions',
            '05:00PM Networking Event',
        ]

        expect(formatGridByTrack(tracks, talks)).toEqual(result)

    })




    it('should return a grade by track with track without space', () => {
        
        const tracks = [
            { 
                name: 'Track 1',
                sessions: [{
                    starts_at: 0,
                    lenght: 5,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 5,
                    final_event: '05:00PM Networking Event'
                }]
            },
            { 
                name: 'Track 2',
                sessions: [{
                    starts_at: 0,
                    lenght: 5,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 5,
                    final_event: '05:00PM Networking Event'
                }]
            }
        ]

        const talks = [
            ['Writing Fast Tests Against Enterprise Rails', 5],
            ['Rails for Python Developers', 5],
            ['Lua for the Masses', 5],
            ['Ruby Errors from Mismatched Gem Versions', 5],
            ['A great talk without space in your schedule', 5]
        ]

        const result = [
            'Track 1',
            '12:00AM Writing Fast Tests Against Enterprise Rails',
            '12:00PM Lunch',
            '01:00PM Rails for Python Developers',
            '05:00PM Networking Event',
            'Track 2',
            '12:00AM Lua for the Masses',
            '12:00PM Lunch',
            '01:00PM Ruby Errors from Mismatched Gem Versions',
            '05:00PM Networking Event',
            'TALKS WITHOUT SPACE IN YOUR CONFERENCE:',
            'A great talk without space in your schedule'
        ]

        expect(formatGridByTrack(tracks, talks)).toEqual(result)

    })


    
    it('should return a event grade without talks', () => {
        
        const tracks = [
            { 
                name: 'Track 1',
                sessions: [{
                    starts_at: 540,
                    lenght: 180,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 240,
                    final_event: '05:00PM Networking Event'
                }]
            },
            { 
                name: 'Track 2',
                sessions: [{
                    starts_at: 540,
                    lenght: 180,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 240,
                    final_event: '05:00PM Networking Event'
                }]
            }
        ]

        const talks = [
            ['Writing Fast Tests Against Enterprise Rails 60min', 60],
            ['Overdoing it in Python 45min', 45],
            ['Lua for the Masses 30min', 30],
            ['Ruby Errors from Mismatched Gem Versions 45min', 45],
            ['Common Ruby Errors 45min', 45],
            ['Rails for Python Developers', 5],
            ['Communicating Over Distance 60min', 60],
            ['Accounting-Driven Development 45min', 45],
            ['Woah 30min', 30],
            ['Sit Down and Write 30min', 30],
            ['Pair Programmin vs Noise 45min', 45],
            ['Rails Magic 60min', 60],
            ['Ruby on Rails: Why We Should Move On 60min', 60],
            ['Clojure Ate Scala (on my project) 45min', 45],
            ['Programming in the Boondocks of Seattle 30min', 30],
            ['Ruby vs. Clojure for Back-End Development 30min', 30],
            ['Ruby on Rails Legacy App Maintenance 60min', 60],
            ['A World Without HackerNews 30min', 30],
            ['User Interface CSS in Rails Apps 30min', 30],
            ['User Interface CSS in Rails Apps 30min (2)', 30],
            ['User Interface CSS in Rails Apps 30min (3)', 30],
            ['User Interface CSS in Rails Apps 30min (4)', 30]
        ]

        const result = [
            "Track 1",
            "09:00AM Writing Fast Tests Against Enterprise Rails 60min",
            "10:00AM Overdoing it in Python 45min",
            "10:45AM Lua for the Masses 30min",
            "11:15AM Ruby Errors from Mismatched Gem Versions 45min",
            "12:00PM Lunch",
            "01:00PM Common Ruby Errors 45min",
            "01:45PM Rails for Python Developers",
            "01:50PM Communicating Over Distance 60min",
            "02:50PM Accounting-Driven Development 45min",
            "03:35PM Woah 30min",
            "04:05PM Sit Down and Write 30min",
            "05:00PM Networking Event",
            "Track 2",
            "09:00AM Pair Programmin vs Noise 45min",
            "09:45AM Rails Magic 60min",
            "10:45AM Ruby on Rails: Why We Should Move On 60min",
            "12:00PM Lunch",
            "01:00PM Clojure Ate Scala (on my project) 45min",
            "01:45PM Programming in the Boondocks of Seattle 30min",
            "02:15PM Ruby vs. Clojure for Back-End Development 30min",
            "02:45PM Ruby on Rails Legacy App Maintenance 60min",
            "03:45PM A World Without HackerNews 30min",
            "04:15PM User Interface CSS in Rails Apps 30min",
            "05:00PM Networking Event",
            "TALKS WITHOUT SPACE IN YOUR CONFERENCE:",
            "User Interface CSS in Rails Apps 30min (2)",
            "User Interface CSS in Rails Apps 30min (3)",
            "User Interface CSS in Rails Apps 30min (4)",
        ]

        expect(formatGridByTrack(tracks, talks)).toEqual(result)

    })

    
    it('should return a event grid with 3 tracks', () => {
        
        const tracks = [
            { 
                name: 'Track 1',
                sessions: [{
                    starts_at: 0,
                    lenght: 10,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 10,
                    final_event: '05:00PM Networking Event'
                }]
            },
            { 
                name: 'Track 2',
                sessions: [{
                    starts_at: 0,
                    lenght: 10,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 10,
                    final_event: '05:00PM Networking Event'
                }]
            },
            { 
                name: 'Track 3',
                sessions: [{
                    starts_at: 0,
                    lenght: 10,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 10,
                    final_event: '05:00PM Networking Event'
                }]
            }
        ]

        const talks = [
            ['Writing Fast Tests Against Enterprise Rails 10min', 10],
            ['Overdoing it in Python 10min', 10],
            ['Lua for the Masses 10min', 10],
            ['Ruby Errors from Mismatched Gem Versions 10min', 10],
            ['Common Ruby Errors 10min', 10],
            ['Rails for Python Developers 10min', 10]
        ]

        const result = [
            "Track 1",
            "12:00AM Writing Fast Tests Against Enterprise Rails 10min",
            "12:00PM Lunch",
            "01:00PM Overdoing it in Python 10min",
            "05:00PM Networking Event",
            "Track 2",
            "12:00AM Lua for the Masses 10min",
            "12:00PM Lunch",
            "01:00PM Ruby Errors from Mismatched Gem Versions 10min",
            "05:00PM Networking Event",
            "Track 3",
            "12:00AM Common Ruby Errors 10min",
            "12:00PM Lunch",
            "01:00PM Rails for Python Developers 10min",
            "05:00PM Networking Event",
        ]

        expect(formatGridByTrack(tracks, talks)).toEqual(result)

    })

    
})
