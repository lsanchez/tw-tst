const cli = require('./cli')

describe('cli', () => {

    it('should return error of talk format', () => {
       
        const talk = 'Talk without lenght'
        const config = []
        const database = []

        expect(cli.runSchedule(talk, config, database)).toEqual(false)
    })

    it('should return true', () => {

        const talk = 'Overdoing it in Python 60min'

        const config = [
            { 
                name: 'Track 1',
                sessions: [{
                    starts_at: 0,
                    lenght: 60,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 60,
                    final_event: '05:00PM Networking Event'
                }]
            },
            { 
                name: 'Track 2',
                sessions: [{
                    starts_at: 0,
                    lenght: 60,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 60,
                    final_event: '05:00PM Networking Event'
                }]
            }
        ]

        const database = [
            ['Writing Fast Tests Against Enterprise Rails 60min', 60]
        ]

        const result = [
            "Track 1",
            "12:00AM Writing Fast Tests Against Enterprise Rails 60min",
            "12:00PM Lunch",
            "01:00PM Overdoing it in Python 60min",
            "05:00PM Networking Event",
            "Track 2",
            "12:00PM Lunch",
            "05:00PM Networking Event"
        ]


        expect(cli.runSchedule(talk, config, database)).toEqual(result)
    })



    it('should return true with lightning/n', () => {

        const talk = 'Overdoing it in Python lightning\n'

        const config = [
            { 
                name: 'Track 1',
                sessions: [{
                    starts_at: 0,
                    lenght: 60,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 60,
                    final_event: '05:00PM Networking Event'
                }]
            },
            { 
                name: 'Track 2',
                sessions: [{
                    starts_at: 0,
                    lenght: 60,
                    final_event: '12:00PM Lunch'
                },
                {
                    starts_at: 780,
                    lenght: 60,
                    final_event: '05:00PM Networking Event'
                }]
            }
        ]

        const database = [
            ['Writing Fast Tests Against Enterprise Rails 60min', 60]
        ]

        const result = [
            "Track 1", 
            "12:00AM Writing Fast Tests Against Enterprise Rails 60min", 
            "12:00PM Lunch", 
            "01:00PM Overdoing it in Python lightning", 
            "05:00PM Networking Event", 
            "Track 2", 
            "12:00PM Lunch", 
            "05:00PM Networking Event"
        ]

        expect(cli.runSchedule(talk, config, database)).toEqual(result)
    })

})