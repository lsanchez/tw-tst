const format_talk = require('../format/talk')

function validateFormat(talk) {

    const formatted_talk = format_talk.format(talk)
    const valid_talk = validateLenght(formatted_talk)
  
    return valid_talk
}

function validateLenght(talk) {
    const talk_lenght = talk[1]

    if (talk_lenght) {
        return true
    } else {
        console.error("Error: Talks need a lenght. Ex: Learn how to KISS 60min")
        return false
    }    
}

module.exports = {
    format: validateFormat,
    lenght: validateLenght
}