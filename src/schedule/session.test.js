const defineTalksSession = require('./session')


describe('defineTalksSession talks', () => {

    it('should format session morning with 60min', () => {

        const morning_length = 60

        const talks = [
            ['Writing Fast Tests Against Enterprise Rails', 60],
            ['Rails for Python Developers', 5]
        ]

        const talks_session = [
            ['Writing Fast Tests Against Enterprise Rails', 60]
        ]

        const talks_over = [
            ['Rails for Python Developers', 5]
        ]

        expect(defineTalksSession(morning_length, talks)).toEqual([talks_session, talks_over])

    })


    it('should format session morning with 180min', () => {

        const morning_length = 180

        const talks = [
            ['Writing Fast Tests Against Enterprise Rails', 60],
            ['Rails for Python Developers', 5],
            ['Lua for the Masses', 30],
            ['Ruby Errors from Mismatched Gem Versions', 45],
            ['Common Ruby Errors', 45],
            ['Rails for Python Developers', 5],
            ['Communicating Over Distance', 60],
            ['Accounting-Driven Development', 45],
            ['Woah', 30],
            ['Sit Down and Write', 30],
            ['Pair Programming vs Noise', 45],
            ['Rails Magic', 60],
            ['Ruby on Rails: Why We Should Move On', 60],
            ['Clojure Ate Scala (on my project)', 45],
            ['Programming in the Boondocks of Seattle', 30]
        ]

        const talks_session = [
            ['Writing Fast Tests Against Enterprise Rails', 60],
            ['Rails for Python Developers', 5],
            ['Lua for the Masses', 30],
            ['Ruby Errors from Mismatched Gem Versions', 45],
            ['Rails for Python Developers', 5],
            ['Woah', 30]
        ]

        const talks_over = [
            ['Common Ruby Errors', 45],
            ['Communicating Over Distance', 60],
            ['Accounting-Driven Development', 45],
            ['Sit Down and Write', 30],
            ['Pair Programming vs Noise', 45],
            ['Rails Magic', 60],
            ['Ruby on Rails: Why We Should Move On', 60],
            ['Clojure Ate Scala (on my project)', 45],
            ['Programming in the Boondocks of Seattle', 30]
        ]

        expect(defineTalksSession(morning_length, talks)).toEqual([talks_session, talks_over])

    })

 
})