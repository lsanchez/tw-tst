const validateTalk = require('./talk')

describe('validate talk format', () => {

    it('should return lenght true', () => {
        const talk = ['Writing Fast Tests Against Enterprise Rails', 60]
        expect(validateTalk.lenght(talk)).toEqual(true)
    })

    it('should return lenght error', () => {
        const talk = ['Writing Fast Tests Against Enterprise Rails']
        expect(validateTalk.lenght(talk))
            .toEqual(false)
    })

    it('should return format false', () => {

        const talk = 'Overdoing it in Python'

        expect(validateTalk.format(talk))
            .toEqual(false)
    })

    it('should return format true', () => {

        const talk = 'Overdoing it in Python 15min'

        expect(validateTalk.format(talk))
            .toEqual(true)
    })

    it('should return format true', () => {

        const talk = 'Overdoing it in Python lightning'

        expect(validateTalk.format(talk))
            .toEqual(true)
    })


})
