const format_talk = require('./talk')

describe('format', () => {

    describe('return raw formatted', () => {
        
        it('should format talks raw data name and length ["Writing ...", 60]', () => {
            const raw = 'Writing Fast Tests Against Enterprise Rails 60min'
            const formatted = ['Writing Fast Tests Against Enterprise Rails 60min', 60]
    
            expect(format_talk.format(raw)).toEqual(formatted)
        })
    
        it('should format talks raw data name and length ["Overdoing ...", 45]', () => {
            const raw = 'Overdoing it in Python 45min'
            const formatted = ['Overdoing it in Python 45min', 45]
    
            expect(format_talk.format(raw)).toEqual(formatted)
        })
    
        it('should format talks raw data name and length ["Rails for...", 5]', () => {
            const raw = 'Rails for Python Developers lightning'
            const formatted = ['Rails for Python Developers lightning', 5]
    
            expect(format_talk.format(raw)).toEqual(formatted)
        })

        it('should format talks raw data with /r/n', () => {
            const raw = 'Rails for Python Developers lightning\r\n'
            const formatted = ['Rails for Python Developers lightning', 5]
    
            expect(format_talk.format(raw)).toEqual(formatted)
        })

        it('should transform lightning in 5', () => {
            const raw = ['Rails', 'for', 'Python', 'Developers', 'lightning']
    
            expect(format_talk.lenght(raw)).toEqual(5)
        })

        it('should transform lightning\n in 5', () => {
            const raw = ['Rails', 'for', 'Python', 'Developers', 'lightning\n']
    
            expect(format_talk.lenght(raw)).toEqual(5)
        })
    
        it('should transform 45min in 45', () => {
            const raw = ['Name', 'of', 'talk', '45min']
    
            expect(format_talk.lenght(raw)).toEqual(45)
        })

    })

})
